+++
draft = false
url = "/presentations/presentations.md"
showthedate = false
+++
# Presentations


### 2015
- [HighEdWeb - Data Silo SMACDown: Enter The EIP](https://jimkittle.gitlab.io/highedweb15)

### 2016
- [HighEdWeb - Mastering Person Data](#)
- The Ohio State University Cyber Security Day - AppSec: The Mgmt Spin

### 2017
- [HighEdWeb - Think Big, Build Small: In the Age Of Digital Transformation](#)
