+++
date = "2017-12-22T16:51:11Z"
draft = false
title = "Kubeadm Quick Hit"
+++

### kubeadm steps

<!-- [kubernetes.io docs](https://kubernetes.io/docs/getting-started-guides/kubespray/) -->

***Master***

- If you are using Kubernetes 1.7 >, swap must be disabled
  - `cat /proc/swaps`
  - `swapoff -a`
  - Remove swap references in `/etc/fstab`
- `echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables`
- Ensure to run `setenforce 0` to account for SELinux
  - [github issue](https://github.com/kubernetes/kubeadm/issues/417)
- Configure flannel
  - `kubeadm init --apiserver-advertise-address=192.168.56.101 --pod-network-cidr=10.244.0.0/16`
  - `sysctl net.bridge.bridge-nf-call-iptables=1`
  - `mkdir -p $HOME/.kube`
  - `cp -i /etc/kubernetes/admin.conf $HOME/.kube/config`
  - `chown $(id -u):$(id -g) $HOME/.kube/config`
  - `kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml`
- If you want to run a single node cluster for development run the following to enable scheduling to master:
  - `kubectl taint nodes --all node-role.kubernetes.io/master-`
- Fix iptables

```shell
systemctl stop kubelet docker
iptables --flush
iptables -tnat --flush
systemctl start docker kubelet
```

***Worker***

- If you are using Kubernetes 1.7 >, swap must be disabled
  - `cat /proc/swaps`
  - `swapoff -a`
  - Remove swap references in `/etc/fstab`

- kubectl to cluster locally
  - copy admin.conf to your local directory
`scp kubemaster1:/etc/kubernetes/admin.conf .`
`kubectl --kubeconfig=admin.conf get nodes`
